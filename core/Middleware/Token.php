<?php

namespace Core\Middleware;

use App\Helpers;

class Token
{
    public function geraToken()
    {
        $header = Helpers::base64ErlEncode('{"alg": "HS256", "typ": "JWT"}');
        $payload = Helpers::base64ErlEncode('{"iat": "'.time().'", "exp": '.(time() + 300).'}');
        // $payload = Helpers::base64ErlEncode('{"iat": "'.time().'", "exp": "1661410000"'.'}');
        $signature = Helpers::base64ErlEncode(
            hash_hmac('sha256', $header.'.'.$payload, 'segredo', true)
        );

        return $header.'.'.$payload.'.'.$signature;
    }

    public function decodeToken($token)
    {
        return Helpers::base64ErlDecode($token);
    }
}
