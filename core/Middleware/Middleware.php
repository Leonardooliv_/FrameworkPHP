<?php

namespace Core\Middleware;

class Middleware
{
    private static $map = [];
    private static $default = [];
    private $middlewares = [];
    private $controller;
    private $params = [];

    public function __construct($middlewares, $controller, $params)
    {
        if (isset($middlewares[0])) {
            $this->middlewares = array_merge(self::$default, $middlewares);
        } else {
            $this->middlewares = array_merge(self::$default);
        }
        $this->controller = $controller;
        $this->params = $params;
    }

    public static function setMap($map)
    {
        self::$map = $map;
    }

    public static function setDefault($default)
    {
        self::$default = $default;
    }

    public function next($request)
    {
        if (empty($this->middlewares)) {
            return call_user_func_array($this->controller, $this->params);
        }

        $middleware = array_shift($this->middlewares);
        $middleware = $middleware;

        if (!isset(self::$map[$middleware])) {
            http_response_code(404);

            exit(json_encode(['status' => 'Middleware não pôde ser processado!']));
        }
        $queue = $this;
        $next = function ($request) use ($queue) {
            return $queue->next($request);
        };

        return (new self::$map[$middleware]())->handle($request, $next);
    }
}
