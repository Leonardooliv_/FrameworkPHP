<?php

namespace Core\Middleware;

class Maintenance
{
    public const MANUTENCAO = false;

    public function handle($request, $next)
    {
        if (self::MANUTENCAO == true) {
            print json_encode(['Página em manutenção. Tente novamente mais tarde.']);

            exit;
        }

        return $next($request);
    }
}
