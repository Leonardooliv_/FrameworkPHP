<?php

namespace Core\Middleware;

use Core\Middleware\Token;
use Library\Entity\Auth\Auth;

class LoginValidate
{
    private $auth;
    private $token;

    public function __construct()
    {
        $this->auth = new Auth();
        $this->token = new Token();
    }

    public function handle($request, $next)
    {
        $this->authLogin();

        return $next($request);
    }

    public function authLogin()
    {
        return $this->verificaLogin();
    }

    public function verificaLogin()
    {
        $user = $_SERVER['PHP_AUTH_USER'];
        $pass = $_SERVER['PHP_AUTH_PW'];

        $usuarioLogado = $this->auth->getLogin($user, $pass);
        $usuarioLogado = $usuarioLogado[0];

        if (!$user && !$pass) {
            print json_encode('Usuario e senha não informado');

            exit;
        }
        if ($user == $usuarioLogado->usuario && $pass == $usuarioLogado->pass) {
            $token = $this->token->geraToken();
            print json_encode($token[0]);
        } else {
            print json_encode('Acesso negado!');

            exit;
        }
    }
}
