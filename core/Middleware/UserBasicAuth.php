<?php

namespace Core\Middleware;

use App\Helpers;
use Core\Middleware\Token;
use Library\Entity\Auth\Auth;

class UserBasicAuth
{
    private $auth;
    private $token;

    public function __construct()
    {
        $this->auth = new Auth();
        $this->token = new Token();
    }

    public function handle($request, $next)
    {
        $this->basicAuth();

        return $next($request);
    }

    public function basicAuth()
    {
        return $this->verificaToken();
    }

    public static function verificaToken()
    {
        $token = getallheaders()['Authorization'];
        $token = str_replace([' ', 'Bearer'], '', $token);
        $parts = explode('.', $token);
        $signature = Helpers::base64ErlEncode(
            hash_hmac('sha256', $parts[0].'.'.$parts[1], 'segredo', true)
        );
        $token = new Token();
        $payload = $token->decodeToken($parts[1]);
        $payloadDecode = json_decode($payload);
        $now = time();
        if ($signature == $parts[2] && $payloadDecode->exp > $now) {
            return [
                'status' => 'Sucesso!',
                'mensagem' => 'Token encontrado e válido',
            ];
        }

        exit(print json_encode([
            'status' => 'Erro!',
            'mensagem' => 'Token inválido',
        ]));
    }
}
