<?php

use Core\Middleware\Middleware;
use Core\Router;

$routes = require '../app/Config/Routes.php';

Middleware::setMap([
    'maintenance' => Core\Middleware\Maintenance::class,
    'UserBasicAuth' => Core\Middleware\UserBasicAuth::class,
    'LoginValidate' => Core\Middleware\LoginValidate::class,
]);

Middleware::setDefault([
    'maintenance',
]);

new Router($routes);
