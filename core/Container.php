<?php

namespace Core;

use Library\Db\Connection;

class Container
{
    public static function controller($controller)
    {
        try {
            $objController = 'App\\Controller\\'.$controller;

            return new $objController();
        } catch (\Throwable $th) {
            http_response_code(404);

            exit(json_encode([
                'status' => 'Erro!',
                'mensagem' => 'Controller não encontrado',
                'catch' => "\n".$th,
            ]));
        }
    }

    public static function pageNotFound()
    {
        http_response_code(404);

        exit(print json_encode([
            'status' => 'Erro!',
            'mensagem' => 'Página não encontrada',
        ]));
    }
}
