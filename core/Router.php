<?php

namespace Core;

use Core\Container;
use Core\Middleware\Middleware;
use stdClass;

class Router
{
    private $routes;
    private $request;

    public function __construct(array $routes)
    {
        $this->request = $_REQUEST;
        $this->setRoutes($routes);
        $this->initialize();
    }

    private function setRoutes($routes)
    {
        foreach ($routes as $route) {
            $routeAr = explode('@', $route[1]);
            if ($route[2]) {
                $middleware = $route[2];
                $routeFinal[] = [$route[0], $routeAr[0], $routeAr[1], $middleware];
            } else {
                $routeFinal[] = [$route[0], $routeAr[0], $routeAr[1]];
            }
        }
        $this->routes = $routeFinal;
    }

    private function responsePost($post)
    {
        return json_decode(json_encode($post));
    }

    private function getRequest()
    {
        $request = $this->getRequestMethod();
        $obj = new stdClass();

        if ($request == 'GET') {
            $obj->method->GET = $request;
            $obj->data = $_GET;
        }
        if ($request == 'POST') {
            $obj->method->POST = $request;
            $obj->data = json_decode(file_get_contents('php://input'));
            $obj->data = $obj->data ?? $this->responsePost($_POST);
        }
        if ($request == 'DELETE') {
            $obj->method->DELETE = $request;
        }
        if ($request == 'PUT') {
            $obj->method->PUT = $request;
        }

        return $obj;
    }

    private function getUrl()
    {
        return parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    }

    private function getRequestMethod()
    {
        return $_SERVER['REQUEST_METHOD'];
    }

    private function initialize()
    {
        $controller = '';
        $action = '';
        $boRouteEcontrada = false;
        $url = $this->getUrl();
        $urlAr = explode('/', $url);

        if (count($urlAr) > 2 && !end($urlAr)) {
            $url = substr($this->getUrl(), 0, -1);
            $urlAr = explode('/', $url);
        }

        foreach ($this->routes as $route) {
            $routeAr = explode('/', $route[0]);
            $params = [];
            for ($i = 0; $i < count($routeAr); $i++) {
                if (strpos($routeAr[$i], '{') !== false && count($urlAr) == count($routeAr)) {
                    $routeAr[$i] = $urlAr[$i];
                    $params[] = $urlAr[$i];
                }
                $route[0] = implode('/', $routeAr);
            }

            if ($url == $route[0]) {
                $boRouteEcontrada = true;
                $controller = $route[1];
                $action = $route[2];
                $middleware[] = $route[3];

                $i = new Middleware($middleware, $controller, $params);
                $i->next($this->request);

                break;
            }
        }

        if ($boRouteEcontrada) {
            $controller = Container::controller($controller);

            try {
                if (count($params) == 1) {
                    $controller->{$action}($params[0], $this->getRequest());
                }

                if (count($params) == 2) {
                    $controller->{$action}($params[0], $params[1], $this->getRequest());
                }

                if (count($params) == 3) {
                    $controller->{$action}($params[0], $params[1], $params[2], $this->getRequest());
                }

                if (!$params) {
                    $controller->{$action}($this->getRequest());
                }

                return;
            } catch (\Throwable $th) {
                http_response_code(404);

                print json_encode([
                    'status' => 'Erro!',
                    'mensagem' => "Problemas com a Action {$action}",
                    'catch' => "\n".$th,
                ]);

                exit;
            }
        }

        return Container::pageNotFound();
    }
}
