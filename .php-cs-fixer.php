<?php

return (new PhpCsFixer\Config())
    ->setRules(['@PhpCsFixer' => true,
        'no_unused_imports' => false,
        'no_mixed_echo_print' => ['use' => 'print'],
        'echo_tag_syntax' => ['format' => 'short'],
        'increment_style' => ['style' => 'post'],
        'yoda_style' => false,
    ])
    ->setLineEnding("\n")
;
