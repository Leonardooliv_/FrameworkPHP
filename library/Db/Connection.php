<?php

namespace Library\Db;

use PDO;
use PDOException;

class Connection
{
    public static function getConection()
    {
        $connection = include __DIR__.'/../../app/Config/global.php';

        $driver = $connection->driver;
        $host = $connection->host;
        $dbname = $connection->dbname;
        $port = $connection->port;
        $user = $connection->username;
        $password = $connection->password;

        try {
            $pdo = new PDO("{$driver}:host={$host};port={$port};dbname={$dbname}", $user, $password);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);

            return $pdo;
        } catch (PDOException $e) {
            exit(json_encode([
                'status' => 'Problemas em conectar ao banco!',
                'mensagem' => $e->getMessage(),
                'catch' => "\n".$e,
            ]));
        }
    }
}
