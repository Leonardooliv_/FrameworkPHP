<?php

namespace Library\Entity\Auth;

use Library\Crud\Crud;

class Auth extends Crud
{
    public function __construct()
    {
        $this->primary_key = 'id';
        $this->schema = 'auth';
        $this->table = 'token';
    }

    public function getLogin($user, $pass)
    {
        return $this->select()
            ->where(['usuario', $user])
            ->where(['senha', $pass])
            ->first()
        ;
    }
}
