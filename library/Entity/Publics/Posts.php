<?php

namespace Library\Entity\Publics;

use App\Helpers;
use Library\Crud\Crud;
use PDOException;

class Posts extends Crud
{
    public function __construct()
    {
        $this->schema = 'public';
        $this->table = 'pessoa';
        $this->primary_key = 'id';
    }

    public function _listar($id = null)
    {
        if ($id) {
            return $this->select($this->schema, $this->table)
                ->where(['id', '=', $id])
                ->get()
            ;
        }

        return $this->select()
            ->get()
        ;
    }

    public function _inserir($data)
    {
        try {
            $this->insert()
                ->into($data)
                ->values($data)
                ->get()
            ;

            return [
                'status' => 'Sucesso!',
                'mensagem' => 'Registro inserido com sucesso!',
            ];
        } catch (PDOException $th) {
            return [
                'status' => 'Erro!',
                'mensagem' => $th->getMessage(),
                'catch' => "\n".$th,
            ];
        }
    }

    public function _alterar($id, $data)
    {
        $query = "UPDATE {$this->schema}.{$this->table} SET ".Helpers::formatarCamposSql($data)." WHERE {$this->primary_key} = {$id}";

        $this->update()
            ->set([$data])
            ->where(['id', $id])
            ->get()
        ;

        return [
            'status' => 'Sucesso!',
            'mensagem' => 'Registro alterado com sucesso.',
        ];
    }

    public function _apagar($id)
    {
        $this->delete($this->schema, $this->table)
            ->where(['id', $id])
            ->get()
        ;

        return [
            'status' => 'Sucesso!',
            'mensagem' => 'Registro removido com sucesso!',
        ];
    }
}
