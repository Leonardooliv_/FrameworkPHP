<?php

declare(strict_types=1);

use Library\Crud\Select;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @coversNothing
 */
final class OrderTest extends TestCase
{
    private $sql;

    protected function setUp(): void
    {
        $this->sql = new Select();
    }

    public function testOrder(): void
    {
        $output = $this->sql->selectTable('rh', 'pessoa')
            ->order(['id', 'desc'])
            ->order(['nome', 'asc'])
            ->getSqlRaw()
        ;
        $this->assertEquals('SELECT * FROM rh.pessoa ORDER BY id desc, nome asc', $output);
    }

    public function testOrderWithoutDirectionShouldBeAsc(): void
    {
        $output = $this->sql->selectTable('rh', 'pessoa')
            ->order(['id'])
            ->getSqlRaw()
        ;
        $this->assertEquals('SELECT * FROM rh.pessoa ORDER BY id asc', $output);
    }
}
