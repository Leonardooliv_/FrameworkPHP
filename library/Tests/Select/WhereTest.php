<?php

declare(strict_types=1);

use Library\Crud\Select;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @coversNothing
 */
final class WhereTest extends TestCase
{
    private $sql;

    protected function setUp(): void
    {
        $this->sql = new Select();
    }

    public function testSimpleWhere(): void
    {
        $id = 67;
        $output = $this->sql->selectTable('rh','pessoa')
            ->where(['id', '=', $id])
            ->getSqlRaw()
        ;
        $this->assertEquals("SELECT * FROM rh.pessoa WHERE id = {$id}", $output);
    }

    public function testManyWhere(): void
    {
        $id = 67;
        $nome = 'João';
        $output = $this->sql->selectTable('rh','pessoa')
            ->where(['id', '=', $id])
            ->where(['nome', '=', "{$nome}"])
            ->getSqlRaw()
        ;
        $this->assertEquals("SELECT * FROM rh.pessoa WHERE id = {$id} AND nome = '{$nome}'", $output);
    }

    public function testWhereWithoutOperatorShouldBeEqual(): void
    {
        $output = $this->sql->selectTable('rh','pessoa')
            ->where(['id', 67])
            ->getSqlRaw()
        ;
        $this->assertEquals('SELECT * FROM rh.pessoa WHERE id = 67', $output);
    }

    public function testWhereWithIn(): void
    {
        $output = $this->sql->selectTable('rh','pessoa')
            ->where(['id', 'in', [1, 2, 67]])
            ->getSqlRaw()
        ;
        $this->assertEquals('SELECT * FROM rh.pessoa WHERE id IN (1, 2, 67)', $output);
    }

    public function testWhereError(): void
    {
        try {
            $this->sql->selectTable('rh','pessoa')
                ->where(['id'])
                ->getSqlRaw()
            ;
        } catch (\Throwable $th) {
            $this->assertEquals('Faltando parametros no where', $th->getMessage());
        }
    }
}
