<?php

declare(strict_types=1);

use Library\Crud\Select;
use PHPUnit\Framework\TestCase;

final class GroupByTest extends TestCase
{
    private $sql;

    protected function setUp(): void
    {
        $this->sql = new Select();
    }

    public function testBasicGroupBy()
    {
        $output = $this->sql->selectTable('rh','pessoa')->group(['id', 'nome'])->getSqlRaw();
        $this->assertEquals("SELECT * FROM rh.pessoa GROUP BY id, nome", $output);
    }
    
    public function testGroupByWithWhere(): void
    {
        $output = $this->sql->selectTable('rh','pessoa')->where(['id', '=', 67])->group(['id', 'nome'])->getSqlRaw();
        $this->assertEquals("SELECT * FROM rh.pessoa WHERE id = 67 GROUP BY id, nome", $output);
    }
}