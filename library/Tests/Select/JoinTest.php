<?php

declare(strict_types=1);

use Library\Crud\Select;
use PHPUnit\Framework\TestCase;

final class JoinTest extends TestCase
{
    private $sql;

    protected function setUp(): void
    {
        $this->sql = new Select();
    }

    public function testJoin(): void
    {
        $output = $this->sql->selectTable('rh','pessoa')
            ->join('rh.documento', 'rh.pessoa.id = rh.documento.id_pessoa')
            ->getSqlRaw();
        $this->assertEquals("SELECT * FROM rh.pessoa INNER JOIN rh.documento ON rh.pessoa.id = rh.documento.id_pessoa", $output);
    }

    public function testWithTwoJoins(): void
    {
        $output = $this->sql->selectTable('rh','pessoa')
            ->join('rh.documento', 'rh.pessoa.id = rh.documento.id_pessoa')
            ->join('rh.test', 'rh.test.id = rh.documento.id_pessoa')
            ->getSqlRaw();
        $this->assertEquals("SELECT * FROM rh.pessoa INNER JOIN rh.documento ON rh.pessoa.id = rh.documento.id_pessoa INNER JOIN rh.test ON rh.test.id = rh.documento.id_pessoa", $output);
    }
}
