<?php

declare(strict_types=1);

use Library\Crud\Select;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @coversNothing
 */
/*
final class OffsetTest extends TestCase
{

    private $sql;

    protected function setUp(): void
    {
        $this->sql = new Select();
    }

    public function testBasicOffset(): void
    {
        $offset = 10;

        $output = $this->sql->selectTable('rh','pessoa')->offset($offset)->getSqlRaw();
        $this->assertEquals("SELECT * FROM rh.pessoa OFFSET {$offset}", $output);
    }

    public function testOffsetWithWhere(): void
    {
        $offset = 10;
        $output = $this->sql->selectTable('rh','pessoa')->where(['id', '=', 67])->offset($offset)->getSqlRaw();
        $this->assertEquals("SELECT * FROM rh.pessoa WHERE id = 67 OFFSET {$offset}", $output);
    }
}

*/