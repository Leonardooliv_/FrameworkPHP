<?php

declare(strict_types=1);

use Library\Crud\Select;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @coversNothing
 */
final class ColumnsTest extends TestCase
{
    private $sql;

    protected function setUp(): void
    {
        $this->sql = new Select();
    }

    public function testIfColumnsIsEmptyShouldBeAll(): void
    {
        $output = $this->sql->selectTable('rh','pessoa')->getSqlRaw();
        $this->assertEquals('SELECT * FROM rh.pessoa', $output);
    }

    public function testColumns(): void
    {
        $output = $this->sql->selectTable('rh','pessoa')
            ->columns(['id', 'nome'])
            ->getSqlRaw()
        ;
        $this->assertEquals('SELECT id, nome FROM rh.pessoa', $output);
    }
}
