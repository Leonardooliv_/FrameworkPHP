<?php

declare(strict_types=1);

use Library\Crud\Select;
use PHPUnit\Framework\TestCase;

final class LimitTest extends TestCase
{
    private $sql;

    protected function setUp(): void
    {
        $this->sql = new Select();
    }

    public function testLimit(): void
    {
        $limit = 10;
        $output = $this->sql->selectTable('rh','pessoa')
            ->limit($limit)
            ->getSqlRaw()
        ;
        $this->assertEquals("SELECT * FROM rh.pessoa LIMIT $limit", $output);
    }
    
    public function testLimitWithWhere(): void
    {
        $limit = 10;
        $output = $this->sql->selectTable('rh','pessoa')
            ->where(['id', '=', 67])
            ->limit($limit)
            ->getSqlRaw()
        ;
        $this->assertEquals("SELECT * FROM rh.pessoa WHERE id = 67 LIMIT $limit", $output);
    }
}
