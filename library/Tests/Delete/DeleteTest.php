<?php

declare(strict_types=1);

use Library\Crud\Delete;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class DeleteTest extends TestCase
{
    private $sql;

    protected function setUp(): void
    {
        $this->sql = new Delete();
    }

    public function testSimpleDelete()
    {
        $output = $this->sql->deleteTable('rh', 'pessoa')->where(['id', '=', 1])->getSqlRaw();
        $this->assertEquals('DELETE FROM rh.pessoa WHERE id = 1', $output);
    }

    public function testDeleteWithLimit()
    {
        $output = $this->sql->deleteTable('rh', 'pessoa')->where(['id', '=', 1])->getSqlRaw();
        $this->assertEquals('DELETE FROM rh.pessoa WHERE id = 1', $output);
    }
}
