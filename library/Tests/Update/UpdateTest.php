<?php

declare(strict_types=1);

use Library\Crud\Update;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @coversNothing
 */
final class UpdateTest extends TestCase
{
    private $sql;

    protected function setUp(): void
    {
        $this->sql = new Update();
    }

    public function testBasicUpdate(): void
    {
        $output = $this->sql->updateTable('rh', 'pessoa')->set(['id = 1', "nome = 'João da Silva'"])->where(['id', '=', 67])->getSqlRaw();
        $this->assertEquals("UPDATE rh.pessoa SET id = 1, nome = 'João da Silva' WHERE id = 67;", $output);
    }

    public function testUpdateWithLimit(): void
    {
        $output = $this->sql->updateTable('rh', 'pessoa')->set(['id = 1', "nome = 'João da Silva'"])->where(['id', '=', 67])->limit(10)->getSqlRaw();
        $this->assertEquals("UPDATE rh.pessoa SET id = 1, nome = 'João da Silva' WHERE id = 67 LIMIT 10;", $output);
    }

    public function testUpdateErrorWithoutValues(): void
    {
        try {
            $this->sql->updateTable('rh', 'pessoa')
                ->set([])
                ->getSqlRaw()
            ;
        } catch (\Throwable $th) {
            $this->assertEquals('Faltando parametros no set', $th->getMessage());
        }
    }
}
