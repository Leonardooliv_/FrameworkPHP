<?php

namespace App\Controller;

use App\Model\User;

class UserController extends Controller
{
    public function login($params)
    {
        $user = new User();
        $return = $user->data($params);
        $this->response([$return]);
    }

    public function show($id)
    {
    }

    public function index()
    {
    }

    public function store($params)
    {
    }

    public function update($id, $params)
    {
    }

    public function destroy($id)
    {
    }
}
