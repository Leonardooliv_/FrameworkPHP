<?php

namespace App\Controller;

abstract class Controller
{
    abstract public function index();

    abstract public function show($id);

    abstract public function update($id, $params);

    abstract public function destroy($params);

    abstract public function store($id);

    protected function response(array $response, $status = 200)
    {
        http_response_code($status);
        print json_encode($response, JSON_UNESCAPED_UNICODE);
    }
}
