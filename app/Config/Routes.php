<?php

$routes[] = ['/login', 'UserController@login'];
$routes[] = ['/posts', 'PostController@index'];
$routes[] = ['/posts/{id}', 'PostController@show', 'UserBasicAuth'];
$routes[] = ['/posts/{id}/delete', 'PostController@destroy'];
$routes[] = ['/posts/{id}/update', 'PostController@update'];
$routes[] = ['/create', 'PostController@store'];

return $routes;
