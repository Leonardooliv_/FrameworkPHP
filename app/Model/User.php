<?php

namespace App\Model;

use Core\Middleware\Token;
use Library\Crud\Crud;
use Library\Entity\Auth\Auth;

class User extends Crud
{
    private $auth;
    private $token;

    public function __construct()
    {
        $this->auth = new Auth();
        $this->token = new Token();
    }

    public function data($params)
    {
        $params = $params;

        $login = $params->data;

        if (!$login->email || !$login->password) {
            return 'Usuario e senha não informado';
        }

        $login->password = "'".md5($login->password)."'";

        $usuarioLogado = $this->auth->getLogin($login->email, $login->password);

        if (!$usuarioLogado) {
            return 'Acesso negado.';
        }
        $token = $this->token->geraToken();

        return [
            'id' => $usuarioLogado->id_pessoa,
            'apelido' => $usuarioLogado->apelido,
            'token' => $token,
        ];
    }
}
