<?php

namespace App\Model;

abstract class Model
{
    protected array $params;
    protected int $id;

    abstract protected function inserir($params);

    abstract protected function alterar($id, $params);

    abstract protected function apagar($id);

    abstract protected function listar();
}
